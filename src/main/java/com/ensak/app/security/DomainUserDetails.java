package com.ensak.app.security;

import com.ensak.app.domain.User;
import com.ensak.app.domain.enumeration.ROLE;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.w3c.dom.stylesheets.LinkStyle;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

public class DomainUserDetails implements UserDetails {

    private Long id;
    private String username;
    private String password;
    private Boolean isActivated;
    private List<SimpleGrantedAuthority> authorities;

    public DomainUserDetails(User user) {
        id = user.getId();
        username = user.getEmail();
        password = user.getPassword();
        isActivated = user.getIsActivated();
        authorities =
                Arrays.asList(new SimpleGrantedAuthority(user.getRoles().toString()));
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return authorities;
    }

    @Override
    public String getPassword() {
        return password;
    }

    @Override
    public String getUsername() {
        return username;
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return isActivated;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public boolean isAdmin() {
        return authorities.contains(new SimpleGrantedAuthority(ROLE.ADMIN.toString()));
    }
}
