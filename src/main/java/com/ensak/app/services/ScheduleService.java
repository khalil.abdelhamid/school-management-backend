package com.ensak.app.services;

import com.ensak.app.domain.Schedule;
import com.ensak.app.repositories.ScheduleRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class ScheduleService {

    private final static Logger log = LoggerFactory.getLogger(ScheduleService.class);

    private final ScheduleRepository repository;

    @Autowired
    public ScheduleService(ScheduleRepository repository) {
        this.repository = repository;
    }

    public Schedule save(Schedule schedule) {
        log.debug("Request to save schedule");
        return repository.save(schedule);
    }

    public List<Schedule> findAll() {
        log.debug("Request to get all schedules");
        return repository.findAll();
    }

    public Optional<Schedule> findOne(Long id) {
        log.debug("Request to find to schedule with id {}", id);
        return repository.findById(id);
    }

    public void delete(Long id) {
        log.debug("Request to delete schedule with id {}", id);
        repository.deleteById(id);
    }
}
