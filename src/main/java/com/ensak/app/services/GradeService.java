package com.ensak.app.services;

import com.ensak.app.domain.Grade;
import com.ensak.app.repositories.GradeRepository;
import com.ensak.app.security.DomainUserDetails;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class GradeService {

    private final static Logger log = LoggerFactory.getLogger(GradeService.class);

    private final GradeRepository repository;

    @Autowired
    public GradeService(GradeRepository repository) {
        this.repository = repository;
    }

    public Grade save(Grade grade) {
        log.debug("Request to save grade");
        return repository.save(grade);
    }

    public List<Grade> findAll() {
        log.debug("Request to get all grades");
        DomainUserDetails user =
                (DomainUserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        return user.isAdmin() ? repository.findAll() :
                repository.findByStudent(user.getId());
    }

    public Optional<Grade> findOne(Long id) {
        log.debug("Request to find to grade with id {}", id);
        return repository.findById(id);
    }

    public void delete(Long id) {
        log.debug("Request to delete grade with id {}", id);
        repository.deleteById(id);
    }
}
