package com.ensak.app.services;

import com.ensak.app.domain.Absence;
import com.ensak.app.repositories.AbsenceRepository;
import com.ensak.app.security.DomainUserDetails;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class AbsenceService {

    private final static Logger log = LoggerFactory.getLogger(AbsenceService.class);

    private final AbsenceRepository repository;

    @Autowired
    public AbsenceService(AbsenceRepository repository) {
        this.repository = repository;
    }

    public Absence save(Absence absence) {
        log.debug("Request to save absence");
        return repository.save(absence);
    }

    public List<Absence> findAll() {
        log.debug("Request to get all absences");
        DomainUserDetails user =
                (DomainUserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();

        return user.isAdmin() ? repository.findAll() :
                repository.findByStudent(user.getId());
    }

    public Optional<Absence> findOne(Long id) {
        log.debug("Request to find to absence with id {}", id);
        return repository.findById(id);
    }

    public void delete(Long id) {
        log.debug("Request to delete absence with id {}", id);
        repository.deleteById(id);
    }
}
