package com.ensak.app.services;

import com.ensak.app.domain.Course;
import com.ensak.app.repositories.CourseRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class CourseService {

    private final static Logger log = LoggerFactory.getLogger(CourseService.class);

    private final CourseRepository repository;

    @Autowired
    public CourseService(CourseRepository repository) {
        this.repository = repository;
    }

    public Course save(Course course) {
        log.debug("Request to save course");
        return repository.save(course);
    }

    public List<Course> findAll() {
        log.debug("Request to get all courses");
        return repository.findAll();
    }

    public Optional<Course> findOne(Long id) {
        log.debug("Request to find to course with id {}", id);
        return repository.findById(id);
    }

    public void delete(Long id) {
        log.debug("Request to delete course with id {}", id);
        repository.deleteById(id);
    }
}
