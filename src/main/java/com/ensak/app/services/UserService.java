package com.ensak.app.services;

import com.ensak.app.domain.Key;
import com.ensak.app.domain.User;
import com.ensak.app.domain.enumeration.KEYTYPE;
import com.ensak.app.repositories.KeyRespository;
import com.ensak.app.repositories.UserRepository;
import com.ensak.app.web.vm.ChangePasswordDTO;
import com.ensak.app.web.vm.RegisterVM;
import com.ensak.app.web.vm.ResetPassDTO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class UserService {

    private final static Logger log = LoggerFactory.getLogger(UserService.class);

    @Autowired
    private PasswordEncoder passwordEncoder;

    private final UserRepository repository;

    @Autowired
    private KeyRespository keyRespository;

    @Autowired
    public UserService(UserRepository repository) {
        this.repository = repository;
    }

    public User save(User user) {
        log.debug("Request to save user");
        return repository.save(user);
    }

    public List<User> findAll() {
        log.debug("Request to get all users");
        return repository.findAll();
    }

    public Optional<User> findOne(Long id) {
        log.debug("Request to find to user with id {}", id);
        return repository.findById(id);
    }

    public void delete(Long id) {
        log.debug("Request to delete user with id {}", id);
        repository.deleteById(id);
    }

    public User registerUser(RegisterVM registerVM) {
        Optional<User> user = repository.findByEmail(registerVM.getEmail());
        if (user.isPresent())
            throw new RuntimeException("Email already exists");

        // create new user
        User newUser = new User();
        newUser.setEmail(registerVM.getEmail());
        newUser.setFirstName(registerVM.getFirstName());
        newUser.setLastName(registerVM.getLastName());
        String encryptedPassword = passwordEncoder.encode(registerVM.getPassword());
        newUser.setPassword(encryptedPassword);
        repository.save(newUser);

        // generate registration key
        Key key = new Key(newUser, KEYTYPE.ACTIVATE);
        keyRespository.save(key);

        // TODO: send registration confirmation email
        return newUser;
    }

    public void confirmRegistration(String key) throws Exception {
        Optional<Key> result = keyRespository.findByToken(key);
        if ( !result.isPresent() )
            throw new Exception("Key NOT found");

        result.get().getUser().setIsActivated(true);
        keyRespository.deleteById(result.get().getId());
    }

    public void resetPassword(String email) throws Exception {
        Optional<User> user = repository.findByEmail(email);
        if ( !user.isPresent() )
            throw new Exception("Email NOT found");

        Key key = new Key(user.get(), KEYTYPE.RESET);
        keyRespository.save(key);

        // TODO: send reset link via email
    }

    public void confirmResetPassword(ResetPassDTO dto) throws Exception {
        Optional<Key> key = keyRespository.findByToken(dto.getKey());
        if ( !key.isPresent() )
            throw new Exception("Key NOT found");

        User user = key.get().getUser();
        String encodedPassword = passwordEncoder.encode(dto.getNewPassword());
        user.setPassword(encodedPassword);

        repository.save(user);
        keyRespository.deleteById(key.get().getId());
    }

    public User changePassword(ChangePasswordDTO dto) throws Exception {
        Optional<User> user = repository.findByEmail(dto.getEmail());
        if ( !user.isPresent() )
            throw new Exception("User NOT found");

        if ( !passwordEncoder.matches(dto.getOldPassword(), user.get().getPassword()) )
            throw new Exception("Old password is invalid");

        String newPassword = passwordEncoder.encode(dto.getNewPassword());
        user.get().setPassword(newPassword);
        repository.save(user.get());
        return user.get();
    }
}
