package com.ensak.app.services;

import com.ensak.app.domain.Lesson;
import com.ensak.app.repositories.LessonRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class LessonService {

    private final static Logger log = LoggerFactory.getLogger(LessonService.class);

    private final LessonRepository repository;

    @Autowired
    public LessonService(LessonRepository repository) {
        this.repository = repository;
    }

    public Lesson save(Lesson lesson) {
        log.debug("Request to save lesson");
        return repository.save(lesson);
    }

    public List<Lesson> findAll() {
        log.debug("Request to get all lessons");
        return repository.findAll();
    }

    public Optional<Lesson> findOne(Long id) {
        log.debug("Request to find to lesson with id {}", id);
        return repository.findById(id);
    }

    public void delete(Long id) {
        log.debug("Request to delete lesson with id {}", id);
        repository.deleteById(id);
    }
}
