package com.ensak.app.services;

import com.ensak.app.domain.ServiceRequest;
import com.ensak.app.domain.User;
import com.ensak.app.repositories.ServiceRequestRepository;
import com.ensak.app.repositories.UserRepository;
import com.ensak.app.security.DomainUserDetails;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class ServiceRequestService {

    private final static Logger log = LoggerFactory.getLogger(ServiceRequestService.class);

    private final ServiceRequestRepository repository;
    private final UserRepository userRepository;

    @Autowired
    public ServiceRequestService(ServiceRequestRepository repository,
                                 UserRepository userRepository) {
        this.repository = repository;
        this.userRepository = userRepository;
    }

    public ServiceRequest save(ServiceRequest serviceRequest) {
        log.debug("Request to save serviceRequest");
        DomainUserDetails userFromSecurity =
                (DomainUserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        Optional<User> user = userRepository.findById(userFromSecurity.getId());
        if ( !user.isPresent() )
            throw new RuntimeException("User not found");

        user.ifPresent(serviceRequest::setStudent);
        return repository.save(serviceRequest);
    }

    public List<ServiceRequest> findAll() {
        log.debug("Request to get all serviceRequests");
        DomainUserDetails user =
                (DomainUserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        return user.isAdmin() ? repository.findAll() :
                repository.findByStudent(user.getId());
    }

    public Optional<ServiceRequest> findOne(Long id) {
        log.debug("Request to find to serviceRequest with id {}", id);
        return repository.findById(id);
    }

    public void delete(Long id) {
        log.debug("Request to delete serviceRequest with id {}", id);
        repository.deleteById(id);
    }
}
