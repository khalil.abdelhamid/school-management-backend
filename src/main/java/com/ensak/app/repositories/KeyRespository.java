package com.ensak.app.repositories;

import com.ensak.app.domain.Key;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface KeyRespository extends JpaRepository<Key, Long> {

    @Query("from Key k where k.token = :token")
    Optional<Key> findByToken(@Param("token") String token);
}
