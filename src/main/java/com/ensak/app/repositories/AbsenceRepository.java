package com.ensak.app.repositories;

import com.ensak.app.domain.Absence;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface AbsenceRepository extends JpaRepository<Absence, Long> {

    @Query("from Absence a where a.student.id = :student_id")
    List<Absence> findByStudent(@Param("student_id") Long id);
}
