package com.ensak.app.repositories;

import com.ensak.app.domain.ServiceRequest;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ServiceRequestRepository extends JpaRepository<ServiceRequest, Long> {

    @Query("from ServiceRequest s where s.student.id = :student_id")
    List<ServiceRequest> findByStudent(@Param("student_id") Long id);
}
