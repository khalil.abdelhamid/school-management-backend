package com.ensak.app.repositories;

import com.ensak.app.domain.Grade;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface GradeRepository extends JpaRepository<Grade, Long> {

    @Query("from Grade g where g.student.id = :student_id")
    List<Grade> findByStudent(@Param("student_id") Long id);
}
