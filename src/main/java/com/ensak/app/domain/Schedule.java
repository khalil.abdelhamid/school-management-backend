package com.ensak.app.domain;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;

@Entity @Table(name = "schedules")
@NoArgsConstructor @Getter @Setter @ToString
public class Schedule extends AbstractEntity {

    @Id @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotBlank @Max(value = 9) @Min(value = 1)
    @Column(nullable = false, unique = true)
    private Integer semester;

    @NotBlank
    @Pattern(regexp = "\\d\\d\\d\\d/\\d\\d\\d\\d")
    private String year;
}
