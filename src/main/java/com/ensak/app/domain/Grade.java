package com.ensak.app.domain;

import com.ensak.app.domain.enumeration.STATUSNOTE;
import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIdentityReference;
import com.fasterxml.jackson.annotation.ObjectIdGenerator;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

@Entity @Table(name = "grades")
@NoArgsConstructor @Getter @Setter @ToString
public class Grade {

    @Id @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Max(value = 20) @Min(value = 0)
    @Column(nullable = false)
    private Float value;

    @NotNull
    @Column(nullable = false)
    @Enumerated(EnumType.STRING)
    private STATUSNOTE status;

    @ManyToOne
    @JoinColumn(name = "id_course")
    private Course course;

    @ManyToOne
    @JoinColumn(name = "id_student")
    private User student;
}
