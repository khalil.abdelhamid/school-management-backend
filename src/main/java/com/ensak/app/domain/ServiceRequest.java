package com.ensak.app.domain;

import com.ensak.app.domain.enumeration.STATUSREQUEST;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;

@Entity @Table(name = "service_requests")
@NoArgsConstructor @Getter @Setter @ToString
public class ServiceRequest extends AbstractEntity{

    @Id @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotBlank
    @Column(nullable = false)
    private String title;

    @NotBlank
    @Column(nullable = false, length = 1024)
    private String description;

    @NotBlank
    @Column(nullable = false)
    @Enumerated(EnumType.STRING)
    private STATUSREQUEST status = STATUSREQUEST.ENATTENTE;

    @ManyToOne
    @JoinColumn(name = "id_student")
    private User student;
}
