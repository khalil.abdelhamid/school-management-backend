package com.ensak.app.domain;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.Set;

@Entity @Table(name = "courses")
@NoArgsConstructor @Getter @Setter @ToString
public class Course extends AbstractEntity{

    @Id @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotBlank
    @Column(nullable = false)
    private String title;

    @NotBlank @Max(value = 9) @Min(value = 1)
    @Column(nullable = false)
    private Integer semester;

    @OneToMany(mappedBy = "course")
    private Set<Lesson> lessons;

    @ManyToOne
    @JoinColumn(name = "id_professor", nullable = false)
    private User professor;

}
