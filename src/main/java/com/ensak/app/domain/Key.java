package com.ensak.app.domain;

import com.ensak.app.domain.enumeration.KEYTYPE;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.data.annotation.CreatedDate;

import javax.persistence.*;
import java.io.Serializable;
import java.time.Instant;
import java.util.UUID;

@Entity @Table(name = "app_keys")
@NoArgsConstructor @Setter @Getter
public class Key implements Serializable {

    @Id @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(updatable = false)
    @CreatedDate
    private final Instant createdDate = Instant.now();

    @Column(nullable = false)
    private String token;

    @Column(updatable = false, nullable = false)
    @Enumerated(EnumType.STRING)
    private KEYTYPE type;

    @OneToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "user_id")
    private User user;

    public Key(User user, KEYTYPE keytype) {
        this.user = user;
        this.token = UUID.randomUUID().toString();
        this.type = keytype;
    }
}
