package com.ensak.app.domain;

import com.sun.istack.NotNull;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;
import java.time.Instant;

@Entity @Table(name="absences")
@NoArgsConstructor @Getter @Setter @ToString
public class Absence extends AbstractEntity {

    @Id @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    @Column(nullable = false)
    private Instant absenceDate = Instant.now();

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "id_student")
    private User student;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "id_course")
    private Course course;
}
