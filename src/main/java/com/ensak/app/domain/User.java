package com.ensak.app.domain;

import com.ensak.app.domain.enumeration.ROLE;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;

@Entity
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@Table(name = "app_users")
@NoArgsConstructor @Getter @Setter @ToString
public class User extends AbstractEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotBlank
    @Column(nullable = false, unique = true)
    private String email;

    @NotBlank
    @Column(nullable = false)
    @JsonIgnore
    private String password;

    @NotBlank
    @Column(nullable = false)
    private String lastName;

    @NotBlank
    @Column(nullable = false)
    private String firstName;

    @Column(nullable = false)
    @Enumerated(EnumType.STRING)
    private ROLE roles = ROLE.STANDARD;

    @Column(nullable = false)
    @JsonIgnore
    private Boolean isActivated = false;

    @Transient
    @JsonIgnore
    private String fullName;

    public String getFullName() {
        return getLastName() + " " + getFirstName();
    }
}
