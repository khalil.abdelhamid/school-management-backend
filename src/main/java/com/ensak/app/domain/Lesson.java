package com.ensak.app.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;

@Entity @Table(name = "lessons")
@NoArgsConstructor @Getter @Setter @ToString
public class Lesson extends AbstractEntity {

    @Id @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotBlank
    @Column(nullable = false)
    private String title;

    @NotBlank
    @Column(nullable = false)
    private String filename;

    @ManyToOne
    @JoinColumn(name = "id_course")
    @JsonIgnore
    private Course course;
}
