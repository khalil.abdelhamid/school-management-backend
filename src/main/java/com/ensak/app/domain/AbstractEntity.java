package com.ensak.app.domain;

import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.Column;
import javax.persistence.EntityListeners;
import javax.persistence.MappedSuperclass;
import java.io.Serializable;
import java.time.Instant;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

/*
Abstract entity which hold created and last modified date

@MappedSuperclass: current entity config ... are applied to subclass, so do NOT create
    a table for this class
@EntityListeners: Inject method defined in class in the lifecycle event of entity
AuditingEntityListener.class: Class defined by spring data which contains lifecycle methods
    annotated with @PreUpdate, @PostUpdate, @PrePersist ....
 */
@MappedSuperclass
@EntityListeners(AuditingEntityListener.class)
@NoArgsConstructor @Getter @Setter @ToString
public abstract class AbstractEntity implements Serializable {

    @Column(name = "created_date", updatable = false)
    @CreatedDate
    protected Instant createdDate = Instant.now();

    @Column(name = "last_modified_date")
    @LastModifiedDate
    protected Instant lastModifiedDate = Instant.now();
}
