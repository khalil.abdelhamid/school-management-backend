package com.ensak.app.domain.enumeration;

public enum ROLE {
    STANDARD, ADMIN, PROFESSOR, STUDENT,
}
