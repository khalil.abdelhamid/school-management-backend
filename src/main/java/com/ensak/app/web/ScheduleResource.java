package com.ensak.app.web;

import com.ensak.app.domain.Schedule;
import com.ensak.app.services.ScheduleService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.constraints.NotNull;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping(value = "api/v1")
public class ScheduleResource {

    private final static Logger log = LoggerFactory.getLogger(ScheduleResource.class);

    private final ScheduleService service;

    @Autowired
    public ScheduleResource(ScheduleService service) {
        this.service = service;
    }

    @GetMapping(value = "/schedules")
    public List<Schedule> getAllSchedules() {
        log.debug("GET request to get all schedules");
        return service.findAll();
    }

    @GetMapping("/schedules/{id}")
    public ResponseEntity<Schedule> getSchedule(@PathVariable (value = "id") @NotNull Long id) {
       log.debug("GET to get schedule with id {} ", id);
       Optional<Schedule> schedule = service.findOne(id);
       return ResponseEntity.of(schedule);
    }

    @PostMapping("/schedules")
    public ResponseEntity<Schedule> createSchedule(@RequestBody Schedule schedule) throws URISyntaxException {
        log.debug("POST request to create schedule {}", schedule);
        if (schedule.getId() != null)
            throw new RuntimeException("A new Schedule cannot already have an ID");

        Schedule result = service.save(schedule);
        return ResponseEntity
                .created(new URI("/api/v1/schedules/" + result.getId()))
                .body(result);
    }

    @PutMapping("/schedules")
    public ResponseEntity<Schedule> updateSchedule(@RequestBody Schedule schedule) {
        log.debug("PUT request to update schedule {}", schedule);
        if (schedule.getId() == null)
            throw new RuntimeException("Schedule id should NOT be null");

        Schedule result = service.save(schedule);
        return ResponseEntity.ok(result);
    }

    @DeleteMapping("/schedules/{id}")
    public ResponseEntity<Void> deleteSchedules(@PathVariable(value = "id") @NotNull Long id){
        log.debug("DELETE request to delete schedule with id {}", id);
        service.delete(id);
        return ResponseEntity.noContent().build();
    }
}
