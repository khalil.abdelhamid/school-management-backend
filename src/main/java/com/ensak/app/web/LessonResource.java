package com.ensak.app.web;

import com.ensak.app.domain.Lesson;
import com.ensak.app.services.LessonService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.Resource;
import org.springframework.core.io.UrlResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.constraints.NotNull;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping(value = "api/v1")
public class LessonResource {

    @Value("${files.directory}")
    private String FILES_DIRECTORY;

    private final static Logger log = LoggerFactory.getLogger(LessonResource.class);

    private final LessonService service;

    @Autowired
    public LessonResource(LessonService service) {
        this.service = service;
    }

    @GetMapping(value = "/lessons")
    public List<Lesson> getAllLessons() {
        log.debug("GET request to get all lessons");
        return service.findAll();
    }

    @GetMapping("/lessons/{id}")
    public ResponseEntity<Lesson> getLesson(@PathVariable (value = "id") @NotNull Long id) {
       log.debug("GET to get lesson with id {} ", id);
       Optional<Lesson> lesson = service.findOne(id);
       return ResponseEntity.of(lesson);
    }

    @GetMapping("/lessons/{id}/download")
    public ResponseEntity downloadLesson(@PathVariable("id") Long id) {
        Optional<Lesson> lesson = service.findOne(id);
        if ( !lesson.isPresent())
            return ResponseEntity.notFound().build();

        Path path = Paths.get(FILES_DIRECTORY + lesson.get().getFilename());
        Resource resource = null;

        try {
            resource = new UrlResource(path.toUri());
        } catch (MalformedURLException e) {
            log.warn("Failed to find with name : " + path.toUri());
            e.printStackTrace();
        }

        return ResponseEntity.ok()
                .header(HttpHeaders.CONTENT_DISPOSITION,
                        "attachement; filename=\"" + resource.getFilename() + "\"")
                .body(resource);
    }

    @PostMapping("/lessons")
    public ResponseEntity<Lesson> createLesson(@RequestBody Lesson lesson) throws URISyntaxException {
        log.debug("POST request to create lesson {}", lesson);
        if (lesson.getId() != null)
            throw new RuntimeException("A new Lesson cannot already have an ID");

        Lesson result = service.save(lesson);
        return ResponseEntity
                .created(new URI("/api/v1/lessons/" + result.getId()))
                .body(result);
    }

    @PutMapping("/lessons")
    public ResponseEntity<Lesson> updateLesson(@RequestBody Lesson lesson) {
        log.debug("PUT request to update lesson {}", lesson);
        if (lesson.getId() == null)
            throw new RuntimeException("Lesson id should NOT be null");

        Lesson result = service.save(lesson);
        return ResponseEntity.ok(result);
    }

    @DeleteMapping("/lessons/{id}")
    public ResponseEntity<Void> deleteLessons(@PathVariable(value = "id") @NotNull Long id){
        log.debug("DELETE request to delete lesson with id {}", id);
        service.delete(id);
        return ResponseEntity.noContent().build();
    }
}
