package com.ensak.app.web;

import com.ensak.app.domain.Absence;
import com.ensak.app.services.AbsenceService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.constraints.NotNull;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping(value = "api/v1")
public class AbsenceResource {

    private final static Logger log = LoggerFactory.getLogger(AbsenceResource.class);

    private final AbsenceService service;

    @Autowired
    public AbsenceResource(AbsenceService service) {
        this.service = service;
    }

    @GetMapping(value = "/absences")
    public List<Absence> getAllAbsences() {
        log.debug("GET request to get all absences");
        return service.findAll();
    }

    @GetMapping("/absences/{id}")
    public ResponseEntity<Absence> getAbsence(@PathVariable (value = "id") @NotNull Long id) {
       log.debug("GET to get absence with id {} ", id);
       Optional<Absence> absence = service.findOne(id);
       return ResponseEntity.of(absence);
    }

    @PostMapping("/absences")
    public ResponseEntity<Absence> createAbsence(@RequestBody Absence absence) throws URISyntaxException {
        log.debug("POST request to create absence {}", absence);
        if (absence.getId() != null)
            throw new RuntimeException("A new Absence cannot already have an ID");

        Absence result = service.save(absence);
        return ResponseEntity
                .created(new URI("/api/v1/absences/" + result.getId()))
                .body(result);
    }

    @PutMapping("/absences")
    public ResponseEntity<Absence> updateAbsence(@RequestBody Absence absence) {
        log.debug("PUT request to update absence {}", absence);
        if (absence.getId() == null)
            throw new RuntimeException("Absence id should NOT be null");

        Absence result = service.save(absence);
        return ResponseEntity.ok(result);
    }

    @DeleteMapping("/absences/{id}")
    public ResponseEntity<Void> deleteAbsences(@PathVariable(value = "id") @NotNull Long id){
        log.debug("DELETE request to delete absence with id {}", id);
        service.delete(id);
        return ResponseEntity.noContent().build();
    }
}
