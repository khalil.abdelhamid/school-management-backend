package com.ensak.app.web.vm;

public class ResetPassDTO {
    private String key;
    private String newPassword;

    public ResetPassDTO() {
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getNewPassword() {
        return newPassword;
    }

    public void setNewPassword(String newPassword) {
        this.newPassword = newPassword;
    }
}

