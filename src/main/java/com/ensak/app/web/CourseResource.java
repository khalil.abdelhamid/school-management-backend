package com.ensak.app.web;

import com.ensak.app.domain.Course;
import com.ensak.app.domain.Lesson;
import com.ensak.app.services.CourseService;
import com.ensak.app.services.LessonService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.constraints.NotNull;
import java.net.URI;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping(value = "api/v1")
public class CourseResource {

    @Value("${files.directory}")
    private String FILES_DIRECTORY;

    private final static Logger log = LoggerFactory.getLogger(CourseResource.class);

    private final CourseService service;
    private final LessonService lessonService;

    @Autowired
    public CourseResource(CourseService service, LessonService lessonService) {
        this.service = service;
        this.lessonService = lessonService;
    }

    @GetMapping(value = "/courses")
    public List<Course> getAllCourses() {
        log.debug("GET request to get all courses");
        return service.findAll();
    }

    @GetMapping("/courses/{id}")
    public ResponseEntity<Course> getCourse(@PathVariable (value = "id") @NotNull Long id) {
       log.debug("GET to get course with id {} ", id);
       Optional<Course> course = service.findOne(id);
       return ResponseEntity.of(course);
    }

    @PostMapping("/courses")
    public ResponseEntity<Course> createCourse(@RequestBody Course course) throws URISyntaxException {
        log.debug("POST request to create course {}", course);
        if (course.getId() != null)
            throw new RuntimeException("A new Course cannot already have an ID");

        Course result = service.save(course);
        return ResponseEntity
                .created(new URI("/api/v1/courses/" + result.getId()))
                .body(result);
    }

    @PostMapping("/courses/{id}/upload-lesson")
    public ResponseEntity uploadLesson(@PathVariable("id") Long id ,
                                       @RequestParam("file") MultipartFile file,
                                       @RequestParam(value = "title", required = false) String title) {

        Optional<Course> course = service.findOne(id);
        if ( !course.isPresent() )
            return ResponseEntity.notFound().build();

        String filename = StringUtils.cleanPath(file.getOriginalFilename());
        Path path = Paths.get(FILES_DIRECTORY + filename);

        try {
            Files.copy(file.getInputStream(), path, StandardCopyOption.REPLACE_EXISTING);

            Lesson lesson = new Lesson();
            lesson.setTitle( title != null ? title : filename);
            lesson.setFilename(filename);

            course.get().getLessons().add(lesson);
            service.save(course.get());
        } catch (Exception e) {
            log.warn("Failed to save file with name : " + filename);
            e.printStackTrace();
        }
        return ResponseEntity.ok().build();
    }

    @PutMapping("/courses")
    public ResponseEntity<Course> updateCourse(@RequestBody Course course) {
        log.debug("PUT request to update course {}", course);
        if (course.getId() == null)
            throw new RuntimeException("Course id should NOT be null");

        Course result = service.save(course);
        return ResponseEntity.ok(result);
    }

    @DeleteMapping("/courses/{id}")
    public ResponseEntity<Void> deleteCourses(@PathVariable(value = "id") @NotNull Long id){
        log.debug("DELETE request to delete course with id {}", id);
        service.delete(id);
        return ResponseEntity.noContent().build();
    }
}
