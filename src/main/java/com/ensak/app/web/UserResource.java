package com.ensak.app.web;

import com.ensak.app.domain.User;
import com.ensak.app.services.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.constraints.NotNull;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping(value = "api/v1")
public class UserResource {

    private final static Logger log = LoggerFactory.getLogger(UserResource.class);

    private final UserService service;

    @Autowired
    public UserResource(UserService service) {
        this.service = service;
    }

    @GetMapping(value = "/users")
    public List<User> getAllUsers() {
        log.debug("GET request to get all Users");
        return service.findAll();
    }

    @GetMapping("/users/{id}")
    public ResponseEntity<User> getUser(@PathVariable (value = "id") @NotNull Long id) {
       log.debug("GET to get User with id {} ", id);
       Optional<User> User = service.findOne(id);
       return ResponseEntity.of(User);
    }

    @PostMapping("/users")
    public ResponseEntity<User> createUser(@RequestBody User User) throws URISyntaxException {
        log.debug("POST request to create User {}", User);
        if (User.getId() != null)
            throw new RuntimeException("A new User cannot already have an ID");

        User result = service.save(User);
        return ResponseEntity
                .created(new URI("/api/v1/Users/" + result.getId()))
                .body(result);
    }

    @PutMapping("/users")
    public ResponseEntity<User> updateUser(@RequestBody User User) {
        log.debug("PUT request to update User {}", User);
        if (User.getId() == null)
            throw new RuntimeException("User id should NOT be null");

        User result = service.save(User);
        return ResponseEntity.ok(result);
    }

    @DeleteMapping("/users/{id}")
    public ResponseEntity<Void> deleteUsers(@PathVariable(value = "id") @NotNull Long id){
        log.debug("DELETE request to delete User with id {}", id);
        service.delete(id);
        return ResponseEntity.noContent().build();
    }
}
