package com.ensak.app.web;

import com.ensak.app.domain.ServiceRequest;
import com.ensak.app.services.ServiceRequestService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.constraints.NotNull;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping(value = "api/v1")
public class ServiceRequestResource {

    private final static Logger log = LoggerFactory.getLogger(ServiceRequestResource.class);

    private final ServiceRequestService service;

    @Autowired
    public ServiceRequestResource(ServiceRequestService service) {
        this.service = service;
    }

    @GetMapping(value = "/service-requests")
    public List<ServiceRequest> getAllServiceRequests() {
        log.debug("GET request to get all serviceRequests");
        return service.findAll();
    }

    @GetMapping("/service-requests/{id}")
    public ResponseEntity<ServiceRequest> getServiceRequest(@PathVariable (value = "id") @NotNull Long id) {
       log.debug("GET to get serviceRequest with id {} ", id);
       Optional<ServiceRequest> serviceRequest = service.findOne(id);
       return ResponseEntity.of(serviceRequest);
    }

    @PostMapping("/service-requests")
    public ResponseEntity<ServiceRequest> createServiceRequest(@RequestBody ServiceRequest serviceRequest) throws URISyntaxException {
        log.debug("POST request to create serviceRequest {}", serviceRequest);
        if (serviceRequest.getId() != null)
            throw new RuntimeException("A new ServiceRequest cannot already have an ID");

        ServiceRequest result = service.save(serviceRequest);
        return ResponseEntity
                .created(new URI("/api/v1/serviceRequests/" + result.getId()))
                .body(result);
    }

    @PutMapping("/service-requests")
    public ResponseEntity<ServiceRequest> updateServiceRequest(@RequestBody ServiceRequest serviceRequest) {
        log.debug("PUT request to update serviceRequest {}", serviceRequest);
        if (serviceRequest.getId() == null)
            throw new RuntimeException("ServiceRequest id should NOT be null");

        ServiceRequest result = service.save(serviceRequest);
        return ResponseEntity.ok(result);
    }

    @DeleteMapping("/service-requests/{id}")
    public ResponseEntity<Void> deleteServiceRequests(@PathVariable(value = "id") @NotNull Long id){
        log.debug("DELETE request to delete serviceRequest with id {}", id);
        service.delete(id);
        return ResponseEntity.noContent().build();
    }
}
