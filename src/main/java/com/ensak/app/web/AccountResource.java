package com.ensak.app.web;

import com.ensak.app.domain.User;
import com.ensak.app.security.jwt.JWTUtil;
import com.ensak.app.services.UserService;
import com.ensak.app.web.vm.ChangePasswordDTO;
import com.ensak.app.web.vm.LoginVM;
import com.ensak.app.web.vm.RegisterVM;
import com.ensak.app.web.vm.ResetPassDTO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.web.bind.annotation.*;

@RestController
public class AccountResource {

    private final static Logger logger = LoggerFactory.getLogger(AccountResource.class);

    @Autowired
    private AuthenticationManager authenticationManager;

    @Qualifier("JPAAuthenticationService")
    @Autowired
    private UserDetailsService userDetailsService;

    @Autowired
    private UserService userService;

    @Autowired
    private JWTUtil jwtUtil;

    @PostMapping("/authenticate")
    public ResponseEntity<String> authenticate(@RequestBody LoginVM login) throws Exception {
        logger.debug("Authentication request with: {}", login);
        try {

            UsernamePasswordAuthenticationToken credentials =
                    new UsernamePasswordAuthenticationToken(
                            login.getEmail(), login.getPassword());

            authenticationManager.authenticate(credentials);
        } catch (BadCredentialsException e) {
            throw new Exception("Incorrect credentials", e);
        }

        UserDetails userDetails = userDetailsService.loadUserByUsername(login.getEmail());
        String token = jwtUtil.generateToken(userDetails);
        return ResponseEntity.ok(token);
    }

    @PostMapping("/register")
    public ResponseEntity<User> register(@RequestBody RegisterVM register) {
        logger.debug("Request to register user: {}", register);
        User user = userService.registerUser(register);

        return ResponseEntity.ok(user);
    }

    @GetMapping("/activate")
    public ResponseEntity<?> activateRegistration(@RequestParam("key") String key) throws Exception {
        userService.confirmRegistration(key);
        return ResponseEntity.noContent().build();
    }

    @PostMapping("/reset-pass/init")
    public ResponseEntity<?> resetPassword(@RequestParam("email") String email) throws Exception {
        userService.resetPassword(email);
        return ResponseEntity.noContent().build();
    }

    @PostMapping("/reset-pass/finish")
    public ResponseEntity<?> confirmResetPassword(@RequestBody ResetPassDTO dto) throws Exception {
        userService.confirmResetPassword(dto);
        return ResponseEntity.noContent().build();
    }

    @PostMapping("/change-pass")
    public ResponseEntity<User> changePassword(@RequestBody ChangePasswordDTO dto) throws Exception {
        User user = userService.changePassword(dto);
        return ResponseEntity.ok(user);
    }
}
