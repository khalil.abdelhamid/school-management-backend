package com.ensak.app.web;

import com.ensak.app.domain.Grade;
import com.ensak.app.services.GradeService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.constraints.NotNull;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping(value = "api/v1")
public class GradeResource {

    private final static Logger log = LoggerFactory.getLogger(GradeResource.class);

    private final GradeService service;

    @Autowired
    public GradeResource(GradeService service) {
        this.service = service;
    }

    @GetMapping(value = "/grades")
    public List<Grade> getAllGrades() {
        log.debug("GET request to get all grades");
        return service.findAll();
    }

    @GetMapping("/grades/{id}")
    public ResponseEntity<Grade> getGrade(@PathVariable (value = "id") @NotNull Long id) {
       log.debug("GET to get grade with id {} ", id);
       Optional<Grade> grade = service.findOne(id);
       return ResponseEntity.of(grade);
    }

    @PostMapping("/grades")
    public ResponseEntity<Grade> createGrade(@RequestBody Grade grade) throws URISyntaxException {
        log.debug("POST request to create grade {}", grade);
        if (grade.getId() != null)
            throw new RuntimeException("A new Grade cannot already have an ID");

        Grade result = service.save(grade);
        return ResponseEntity
                .created(new URI("/api/v1/grades/" + result.getId()))
                .body(result);
    }

    @PutMapping("/grades")
    public ResponseEntity<Grade> updateGrade(@RequestBody Grade grade) {
        log.debug("PUT request to update grade {}", grade);
        if (grade.getId() == null)
            throw new RuntimeException("Grade id should NOT be null");

        Grade result = service.save(grade);
        return ResponseEntity.ok(result);
    }

    @DeleteMapping("/grades/{id}")
    public ResponseEntity<Void> deleteGrades(@PathVariable(value = "id") @NotNull Long id){
        log.debug("DELETE request to delete grade with id {}", id);
        service.delete(id);
        return ResponseEntity.noContent().build();
    }
}
