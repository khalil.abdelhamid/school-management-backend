# school-management-app
School management app

# How to use ?

**1. Setup mysql database**
Create a mysql database with the name **GEdb** or change the name on the configuration file called `application.yml`.

You can also run mysql database instance using docker with the following command:
```
docker run --name='mysqldb' -d -p 3306:3306 -e MYSQL_ROOT_PASSWORD=root -e MYSQL_DATABASE=GEdb mysql
```

**2. run spring application**
Open the console on the proejct directory, and run the following command:
```
./mvwn spring-boot:run
```
or
```
mvwn spring-boot:run
```

**3. Access the applicaton**
The application is hosted at `http://localhost:1337/`

Authentication endpoint: `http://localhost:1337/authenticate
Registration endpoint: `http://localhost:1337/register`

API endpoints start with: `http://localhost:1337/api/v1/`, for example to query all course use the following `http://localhost:1337/api/v1/courses` etc.
