FROM maven:3-openjdk-8 as build
WORKDIR /app
COPY . /app
RUN mvn package -Dmaven.test.skip=true

FROM openjdk:8
WORKDIR /app
COPY --from=build /app/target/app-0.0.1-SNAPSHOT.jar /app
CMD ["java", "-jar", "app-0.0.1-SNAPSHOT.jar"]
